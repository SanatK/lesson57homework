//ОБЪЕКТЫ

const newUser = {
    id : "u0001",
    name : "Elon",
    login : "elonmusk@gmail.com",
    password : "123SpaceX",
    isAuthorized : false
}

/* При создании нового поста, в нем будет задаваться идентификационный номер пользователя,
с помощью которого можно будет найти все посты юзера*/

const post = {
    id : "post001",
    body : "It's my first post in JS!",
    likes : 0,
    userId : "u0001"
}

/* При создании комментария, в нем будет задаваться id пользователя и id поста,
с помощью которых можно найти все комментарии определенного поста и узнать какому юзеру
комментарий принадлежит */

const comment = {
    id : "com001",
    body : "New comment",
    postId : "post01",
    userId : "u0001"
}

// МАССИВЫ


const posts = [];

// Метод для добавления в массив posts нового поста

function addPost(id, body, userId){
    const newPost = {
        id : id,
        body : body,
        likes : 0,
        userId : userId
    }
    posts.push(newPost);
}

posts.push(post);
addPost("post002", "Hello World", "u0001");
addPost("post003", "Hello Bishkek", "u0001");

// АВТОРИЗАЦИЯ

console.log(newUser.isAuthorized);

function authorizeUser(user){
    if(user.isAuthorized==false)
    return user.isAuthorized = true;
}

authorizeUser(newUser);
console.log(newUser.isAuthorized);

// Добавление лайка в пост

function addOrRemoveLike(postId){
    for(let i = 0; i<posts.length; i++){
        if(posts[i].id===postId && posts[i].likes==0){
            posts[i].likes++;
        }else if (posts[i].id===postId && posts[i].likes==1){
            posts[i].likes--;
        }
    }
}
addOrRemoveLike("post001");
addOrRemoveLike("post001");
console.log(posts);